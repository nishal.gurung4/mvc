 <?php

$router->get('', 'PagesController@home');
$router->get('speakers', 'PagesController@speakers');
$router->get('sponsors', 'PagesController@sponsors');
$router->get('sponsors/create', 'PagesController@createSponsor');
$router->post('sponsors', 'PagesController@addSponsor');
